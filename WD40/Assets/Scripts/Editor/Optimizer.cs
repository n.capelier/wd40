﻿using UnityEngine;
using UnityEditor;






public static class Optimizer
{

	public static void Optimize ()
	{
	}

}

public class OptimizerWindows : EditorWindow
{

	bool delUnusedCallBack = false;

	[MenuItem ("Tools/Script Optimizer")]
	#region Private Static Methods
	private static void Init ()
	{
		OptimizerWindows window = (OptimizerWindows)GetWindow (typeof(OptimizerWindows), false, "Optimizer");
		/*	window.maxSize = new Vector2 (400, 600);
		window.minSize = window.maxSize;*/
		window.Show ();
	}

	#endregion Private Static Methods

	void OnGUI ()
	{
		GUILayout.Space (10);
		gui_AskForDelUnusedCallBack ();
		GUILayout.Space (5);
		gui_ValidateButton ();
	}

	void gui_AskForDelUnusedCallBack ()
	{
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		delUnusedCallBack = GUILayout.Toggle (delUnusedCallBack, "Delete unused Unity methods ( Start, Update, ...");
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
	}

	void gui_ValidateButton ()
	{
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUI.contentColor = new Color (.6f, 1, 0);
		if (GUILayout.Button ("Optimize all scripts", GUILayout.MaxWidth (150)))
			Optimizer.Optimize ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
	}
}

