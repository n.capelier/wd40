﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Character 01 keyboard controller
    /// </summary>
    public class Player1Controller : MonoBehaviour
    {

        // Player speed
        [Range(0.1f, 1000f)]
        [SerializeField] float speed = 10;

        void Awake()
        {
            
        }
        
        void Start()
        {
            
        }
        
        void Update()
        {
            Move();
        }
        
        void Move()
        {
            if(Input.GetButton("Left"))
            {
                PlayerManager.Instance.rb.velocity = new Vector2(-speed, 0) * Time.deltaTime;
            }
            if (Input.GetButton("Right"))
            {
                PlayerManager.Instance.rb.velocity = new Vector2(speed, 0) * Time.deltaTime;
            }
            if (Input.GetButton("Up"))
            {
                PlayerManager.Instance.rb.velocity = new Vector2(0, speed) * Time.deltaTime;
            }
            if (Input.GetButton("Down"))
            {
                PlayerManager.Instance.rb.velocity = new Vector2(0, -speed) * Time.deltaTime;
            }
        }
        
    }
}