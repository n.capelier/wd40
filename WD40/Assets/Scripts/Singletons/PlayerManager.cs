﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Management;

namespace Game
{
    /// <summary>
    /// General management of the Player object.
    /// </summary>
    public class PlayerManager : Manager<PlayerManager>
    {

        [HideInInspector] public Rigidbody2D rb;

        void Awake()
        {
            MakeSingleton(false);
            rb = GetComponent<Rigidbody2D>();
        }

        void Start()
        {

        }


        void Update()
        {

        }



    }

}