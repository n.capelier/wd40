﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Management;

namespace Game
{
    /// <summary>
    /// General management of the game.
    /// </summary>
    public class GameManager : Manager<GameManager>
    {



        void Awake()
        {
            MakeSingleton(true);
        }

        void Start()
        {

        }


        void Update()
        {

        }



    }
}