﻿using UnityEngine;

public static class RendererExtensions
{

	/*
	 * 
	 * Overview

		This C# class gives simple extension access to checking if an Renderer is rendered by a specific Camera.
		Due to the way UnityScript/javascript compiles extension methods, it's not accessible in the same way, so another javascript specific class is added below it

		Use

		For the c# extension method: renderer.IsVisibleFrom(cam)
		For the UnityScript static function: RendererHelper.IsVisibleFrom(renderer, cam)

		Example - TestRendered.cs

		using UnityEngine;
		 
		public class TestRendered : MonoBehaviour
		{	
			void Update()
			{
				if (renderer.IsVisibleFrom(Camera.main)) Debug.Log("Visible");
				else Debug.Log("Not visible");
			}
		}
	 *
	 *
	 */
	public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
	{
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
		return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
	}
}