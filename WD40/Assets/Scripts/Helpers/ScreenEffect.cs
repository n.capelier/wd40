﻿/*using System.Collections.Generic;
using MovementEffects;
using UnityEngine;
using UnityEngine.UI;

public class ScreenEffects
{
	private static readonly Color s_TransparentColorBlack = new Color (0, 0, 0, 0);
	private static readonly Color s_TransparentColorWhite = new Color (0, 0, 0, 0);

	#region Fading

	/// <summary>
	/// If the screen is currently faded.
	/// </summary>
	public bool IsFaded { get; private set; }

	/// <summary>
	/// The current alpha of the screen fade.
	/// </summary>
	public float CurrentFadeAlpha { get { return CurrentFadeColor.a; } }

	/// <summary>
	/// The current color of the screen fade.
	/// </summary>
	public Color CurrentFadeColor { get; private set; }

	/// <summary>
	/// The remaining percentage until the current fade is done, between [0, 1].
	/// </summary>
	public float RemainingFadePercentage { get; private set; }

	/// <summary>
	/// The remaining time until the fade is done.
	/// </summary>
	public float RemainingFadeDuration { get; private set; }

	/// <summary>
	/// If the current fading is realtime or not.
	/// </summary>
	public bool IsCurrentFadeUnscaledTime { get; private set; }

	/// <summary>
	/// The GameObject for the Canvas used to draw screen fading.
	/// </summary>
	private readonly GameObject _screenFadeCanvas;

	/// <summary>
	/// The actual Image UI element that is used for screen fading.
	/// </summary>
	private readonly Image _screenFadeImage;

	#endregion

	public ScreenEffects ()
	{
		IsFaded = false;
		CreateScreenFadeObject (out _screenFadeCanvas, out _screenFadeImage);
		SetColor (s_TransparentColorBlack);
		Object.DontDestroyOnLoad (_screenFadeCanvas);
	}

	/// <summary>
	/// Fades the screen to a color over a duration.
	/// </summary>
	public void Fade (Color color, float duration, bool unscaledTime = true)
	{
		if (color == Color.black && CurrentFadeColor == s_TransparentColorWhite)
		{
			SetColor (s_TransparentColorBlack);
		}
		Timing.RunCoroutine (Coroutine_LerpToColor (color, duration, unscaledTime));
	}

	/// <summary>
	/// Unfades the screen over a duration.
	/// </summary>
	public void Unfade (float duration, bool unscaledTime = true)
	{
		Timing.RunCoroutine (Coroutine_LerpToColor (s_TransparentColorBlack, duration, unscaledTime));
	}

	/// <summary>
	/// Actually controls fading screen to color over time.
	/// </summary>
	private IEnumerator<float> Coroutine_LerpToColor (Color color, float duration, bool unscaledTime)
	{
		IsCurrentFadeUnscaledTime = unscaledTime;

		Color startingColor = CurrentFadeColor;
		float startTime = unscaledTime ? Time.unscaledTime : Time.time;

		IsFaded = true;

		while ((unscaledTime ? Time.unscaledTime : Time.time) < startTime + duration)
		{
			float t = ModifyDelta (((unscaledTime ? Time.unscaledTime : Time.time) - startTime) / duration);
			Color lerpedColor = Color.Lerp (startingColor, color, t);
			SetColor (lerpedColor);

			// Info
			RemainingFadePercentage = t;
			RemainingFadeDuration = duration - ((unscaledTime ? Time.unscaledTime : Time.time) - startTime);

			yield return Timing.WaitForOneFrame;
		}

		// Final set
		SetColor (color);
		RemainingFadePercentage = 0;

		if (color.a == 0)
		{
			SetColor (s_TransparentColorWhite);
			IsFaded = false;
		}
	}

	/// <summary>
	/// Creates the canvas object and Image object that are used for screen fading.
	/// </summary>
	private void CreateScreenFadeObject (out GameObject screenFadeCanvas, out Image screenFadeImage)
	{
		screenFadeCanvas = new GameObject ("[GENERATED][Screen][Fading] Canvas");
		screenFadeCanvas.AddComponent<RectTransform> ();
		Canvas canvas = screenFadeCanvas.AddComponent<Canvas> ();
		screenFadeCanvas.AddComponent<CanvasScaler> ();

		canvas.renderMode = RenderMode.ScreenSpaceCamera;

		var screenFadeImageGO = new GameObject ("[GENERATED][Screen][Fading] Image");
		screenFadeImageGO.transform.parent = screenFadeCanvas.transform;
		RectTransform imageRectTransform = screenFadeImageGO.AddComponent<RectTransform> ();
		screenFadeImageGO.AddComponent<CanvasRenderer> ();
		screenFadeImage = screenFadeImageGO.AddComponent<Image> ();

		imageRectTransform.anchoredPosition = Vector2.zero;
		imageRectTransform.anchorMin = Vector2.zero;
		imageRectTransform.anchorMax = Vector2.one;
		imageRectTransform.sizeDelta = Vector2.zero;
	}

	/// <summary>
	/// Smooths the beginning near 0 while speeding up the tail end.
	/// </summary>
	private float ModifyDelta (float t)
	{
		// Smooth at the beginning and end
		t = t * t * (3f - 2f * t);
		// Fast at beginning and end
		//t = -1 * (t - 1) * (t - 1) + 1;
		return t;
	}

	/// <summary>
	/// Sets the current color.
	/// </summary>
	private void SetColor (Color color)
	{
		CurrentFadeColor = color;
		_screenFadeImage.color = color;
	}
}*/