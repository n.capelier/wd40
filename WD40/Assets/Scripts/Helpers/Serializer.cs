﻿using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

public static class Serializer
{
	public static T BinaryLoad<T> (string filename) where T: class
	{
		if (File.Exists (filename))
		{
			try
			{
				using (Stream stream = File.OpenRead (filename))
				{
					BinaryFormatter formatter = new BinaryFormatter ();
					return formatter.Deserialize (stream) as T;
				}
			}
			catch (Exception e)
			{
				Debug.Log (e.Message);
			}
		}
		return default(T);
	}

	public static void BinarySave<T> (string filename, T data) where T: class
	{
		using (Stream stream = File.OpenWrite (filename))
		{    
			BinaryFormatter formatter = new BinaryFormatter ();
			formatter.Serialize (stream, data);
		}
	}


	public static T XMLLoad<T> (string filename) where T: class
	{
		if (File.Exists (filename))
		{
			try
			{
				using (Stream stream = File.OpenRead (filename))
				{
					XmlSerializer formatter = new XmlSerializer (typeof(T));
					return formatter.Deserialize (stream) as T;
				}
			}
			catch (Exception e)
			{
				Debug.Log (e.Message);
			}
		}
		return default(T);
	}

	public static void XMLSave<T> (string filename, T data) where T: class
	{
		using (Stream stream = File.OpenWrite (filename))
		{    
			XmlSerializer formatter = new XmlSerializer (typeof(T));
			formatter.Serialize (stream, data);
		}
	}




	public static T JSONLoad<T> (string filename) where T: class
	{
		if (File.Exists (filename))
		{
			try
			{
				using (Stream stream = File.OpenRead (filename))
				{
					
					return JsonUtility.FromJson<T> (File.ReadAllText (filename));
				}
			}
			catch (Exception e)
			{
				Debug.LogError (e.Message);
			}
		}
		return default(T);
	}

	public static void JSONSave<T> (string filename, T data) where T: class
	{
		
		File.WriteAllText (filename, JsonUtility.ToJson (data));
	}
}